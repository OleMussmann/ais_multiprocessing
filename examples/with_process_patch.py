"""
Data is split into chunks and stored in input queue.
Chunks are taken from input queue and processed in parallel. Processed chunks are then stored in output queue.

This patch parallelizes reading and processing as follows:

1. Core 1 starts reading and storing chunks in the input queue.
2. All other cores start taking items from the queue and process them.
3. When Core 1 is done reading, it becomes a worker, taking items from the queue and processing them as well.
"""

from multiprocessing import Queue, Process
import time

def read_to_queue(file_path: str, char_chunk_size: int, item_queue, sentinel: str, no_of_processes: int) -> None:
    """
    Function that reads AIS data, cuts the data into chunks and stores those in a queue.
    To signal the end of the queue, for every worker process, a sentinel is put at the end to signal the end of the queue.

    Args:
        file_path (str): path to file
        char_chunk_size (int): size of data chunks
        item_queue (queue): queue into which data is stored
        sentinel (str): user defined string signalling end of queue
        no_of_worker_processes (int): no of worker processes that need an end-of-queue signal
    """
    with open(file_path, 'r') as data_lines:
        last_line = []
        iterations = 0
        while True:
            iterations += 1
            lines = data_lines.readlines(char_chunk_size)
            chunk = last_line + lines
            if not lines:
                item_queue.put(last_line)
                break
            last_line = []
            while True:
                try:
                    next_line = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] != "!":
                    chunk.append(next_line)
                else:
                    last_line = [next_line]
                    break
            item_queue.put(chunk)
    put_sentinel(no_of_processes, sentinel, item_queue)

def put_sentinel(no_of_processes: int, sentinel: str, item_queue) -> None:
    """
    Put sentinel.

    Args:
        no_of_processes (int): number of processes in need of end-of-queue signal
        sentinel (str): user defined string
        item_queue (queue): queue where sentinel is put
    """
    for _ in range(no_of_processes):
        item_queue.put(sentinel)

def reading_then_processing(file_path: str, char_chunk_size: int, item_queue, sentinel: str, no_of_processes: int, function, output) -> None:
    """
    Complete function for first processor to execute.

    First: read data and store in queue.
    Second: process data like other cores.

    Args:
        file_path (str): path to data
        char_chunk_size (int): size of data chunks
        item_queue ([type]): queue of data chunks waiting to be processed
        sentinel (str): user defined string signalling end of data
        no_of_worker_processes (int): no_of_processors not counting the core needed for reading the data.
        function ([type]): function to be executed.
        output ([type]): location where processed items are stored or await further processing.
    """
    read_to_queue(file_path, char_chunk_size, item_queue, sentinel, no_of_processes)
    queue_to_func(item_queue, function, sentinel, output)

def just_processing(item_queue, function, sentinel: str, output) -> None:
    """
    Function for remaining processors to execute:
    Taking from queue, then processing and storing in output queue

    Args:
        item_queue (queue): queue with items waiting to be processed
        function (function): function to be executed
        sentinel (str): user defined string signalling end of queue
        output (queue): queue where output is stored
    """
    queue_to_func(item_queue, function, sentinel, output)

def queue_to_func(item_queue, function, sentinel: str, output) -> None:
    """
    Function taking items from queue, feeding them to the target function and storing them in the output queue.

    Args:
        item_queue (queue): data chunks waiting to be processed
        function (functio): target function to be executed
        sentinel (str): user defined string signalling the end of the queue
        output (queue): output queue
    """
    items_in_queue = 0
    item = None
    while True:
        items_in_queue += 1
        item = item_queue.get(timeout=.1)
        if item == sentinel:
            break
        output.put(function(item))

def do_something(data_chunk: list) -> list:
    """
    Silly function reversing the lines in the data chunk.

    Args:
        data_chunk (list): list of ais messages

    Returns:
        list: 'processed' data
    """
    results = []

    for i in range(len(data_chunk)):
        time.sleep(0.1) #to illustrate performance difference for different no. of processes
        reversed = data_chunk[i][::-1]
        results.append(reversed)

    return results

def process_parallel(reading_then_processing, just_processing, function, file_path: str, char_chunk_size: int, item_queue, no_of_processes: int, sentinel: str, output) -> None:
    """
    Parallelization of reading and processing of data chunks.

    Args:
        reading_then_processing (function): process for first core: first reading, then processing
        just_processing (function): process for other core, just processing
        function (function): function to be executed.
        file_path (str): path to data
        char_chunk_size (int): size of data chunks
        item_queue ([type]): queue where data awaits processing
        no_of_processes (int): number of processes to be executed
        sentinel (str): user defined string signalling end of queue
        output ([type]): queue where outputs are stored
    """
    processes = []

    processes.append(Process(target=reading_then_processing, args=(file_path, char_chunk_size, item_queue, sentinel, no_of_processes, function, output)))#Create process 1; to be run on one core
    [processes.append(Process(target=just_processing, args=(item_queue, function, sentinel, output))) for _ in range(no_of_processes - 1)]#Create remaining processes; to be run on other cores

    for process in processes: #start processes
        process.start()

    for process in processes: #wait for processes to finish before moving on
        process.join()

def main():

    start = time.perf_counter()

    file_path = "../data/synthetic_data_short.txt"
    char_chunk_size = 10

    no_of_processes = 4

    sentinel = "KILL"

    item_queue = Queue()

    output = Queue()

    results = []

    process_parallel(reading_then_processing, just_processing, do_something, file_path, char_chunk_size, item_queue, no_of_processes, sentinel, output)

    while not output.empty(): #just checking the contents of the queue
        results.append(output.get())

    print(results)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start,2)} seconds') #to evaluate 'performance'

if __name__=='__main__':
    main()
