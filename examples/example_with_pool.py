"""Couldn't get multiprocessing to work with the manual queueing yet, 
so here is an alternative using the "pool" object from the multiprocessing module.
Note: manual queueing might be better in the end if we want processes be able to to talk to each other,
see documentation on multiprocessing."""

def read_to_list(file_path: str, list_of_messages: list):
    """Modification(bit messy) of the read_to_queue function, 
    such that it writes individual AIS messages to a list.
    Why? Because I thought there was no reason to divide into chunks first.
    Please correct me if I'm wrong:)
    
    Args:
        file_path (str): [path to the data]
        list_of_messages (list): [list into which AIS messages will be stored]
    """
    with open(file_path, 'r') as data_lines:
        line = ""
        last_line = ""
        full_message = ""
        while True:
            line = data_lines.readline().rstrip() #read line and strip /n
            try:
                if line[0] != "!": #would throw IndexError at end of data
                    full_message = last_line + " " + line
                elif last_line != "": #prevent first line from being an empty string
                    list_of_messages.append(last_line)
                    full_message = line
                else:
                    full_message = line
            except IndexError:
                list_of_messages.append(full_message)
                break
            if not line:
                list_of_messages.append(last_line)
                break
            last_line = ""
            while True:
                    next_line = data_lines.readline().rstrip()
                    try: 
                        if next_line[0] != "!": #IndexError at end of data
                            full_message = full_message + " " + next_line
                        else:
                            list_of_messages.append(full_message)
                            last_line = next_line
                            break
                    except IndexError:
                            break

def process_parallel(function, data: list, cores_to_use: int) -> list:
    """Paralellization of processing of the ais messages, using the pool class.
    

    Args:
        function (map): function that processes AIS data
        data (list): list of ais messages
        cores_to_use (int): no. of cores one wishes to use for processing

    Returns:
        list: list containing processed data chunks
    """
    from multiprocessing import Pool
    with Pool(cores_to_use) as p:
        processed = [p.map(function, data)]
    return processed

def do_something(data: str) -> str: 
    """Silly function reversing the ais message.

    Args:
        data (list): list of ais messages

    Returns:
        list: 'processed' data
    """
    results = ""
    
    reversed = data[::-1]
    results = reversed
    
    return results

def main():

    import time

    start = time.perf_counter()

    file_path = "../data/synthetic_data.txt"

    list_of_messages = []

    read_to_list(file_path, list_of_messages)

    cores_to_use = 4

    results = process_parallel(do_something, list_of_messages, cores_to_use)

    print(results)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start,2)} seconds')

if __name__ == '__main__':
    main()
