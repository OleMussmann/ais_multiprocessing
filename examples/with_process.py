"""
Data is split into chunks and stored in input queue.
Chunks are taken from input queue and processed in parallel. Processed chunks are then stored in output queue.

Vary no. of processes to see benefits of multiprocessing.
"""

from multiprocessing import Queue, Process
import time

def read_to_queue(file_path: str, char_chunk_size: int, item_queue) -> None:
    with open(file_path, 'r') as data_lines:
        last_line = []
        iterations = 0
        while True:
            iterations += 1
            lines = data_lines.readlines(char_chunk_size)
            chunk = last_line + lines
            if not lines:
                item_queue.put(last_line)
                break
            last_line = []
            while True:
                try:
                    next_line = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] != "!":
                    chunk.append(next_line)
                else:
                    last_line = [next_line]
                    break
            item_queue.put(chunk)


def queue_to_func(item_queue, function, sentinel: str, output) -> None:
    """Function taking items from queue, feeding them to the target function and storing them in the output queue

    Args:
        item_queue (queue): data chunks waiting to be processed
        function (functio): target function to be executed
        sentinel (str): user defined string signalling the end of the queue
        output (queue): output queue
    """
    items_in_queue = 0
    item = None
    while True:
        items_in_queue += 1
        item = item_queue.get(timeout=.1)
        if item == sentinel:
            break
        output.put(function(item))

def do_something(data_chunk: list) -> list:
    """Silly function reversing the lines in the data chunk.

    Args:
        data_chunk (list): list of ais messages

    Returns:
        list: 'processed' data
    """
    results = []

    for i in range(len(data_chunk)):
        time.sleep(0.1) #to illustrate performance difference for different no. of processes
        reversed = data_chunk[i][::-1]
        results.append(reversed)

    return results

def process_parallel(function, item_queue, no_of_processes: int, sentinel: str, output) -> None:
    """Function that parallelizes processing of data chunks

    Args:
        function (function): function that takes items from queue, processes them and stores in output queue
        item_queue (queue): input queue
        no_of_processes (int): self-explanatory
        sentinel (str): user defined string signalling end of queue
        output (queue): output queue where processed chunks are stored
    """
    processes = [Process(target=function, args=(item_queue, do_something, sentinel, output)) for x in range(no_of_processes)]

    for process in processes: #start processes
        process.start()

    for process in processes: #wait for processe to finish before moving on
        process.join()

def main():

    start = time.perf_counter()

    file_path = "../data/synthetic_data_short.txt"
    char_chunk_size = 10 #should have char_chunk_size < len(data) to see performance difference between parallel and serial processing

    no_of_processes = 4

    sentinel = "KILL"

    item_queue = Queue()

    output_queue = Queue()

    results = []

    read_to_queue(file_path, char_chunk_size, item_queue)

    for __ in range(no_of_processes): #for every process, one sentinel is needed to signal the end of the queue
        item_queue.put(sentinel)

    process_parallel(queue_to_func, item_queue, no_of_processes, sentinel, output_queue)

    while not output_queue.empty(): #just checking the contents of the queue
        results.append(output_queue.get())

    print(results)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start,2)} seconds') #to evaluate 'performance' for different no. of processes 

if __name__=='__main__':
    main()
