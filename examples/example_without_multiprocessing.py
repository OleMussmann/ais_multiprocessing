"""For reference, the same example as "example_with_pool.py", but processed serially.
This is actually faster because the function do_something is very simple.

Also, the order of the result is different, 
because parallel processing with pool does not preserve the order of the messages."""

def read_to_list(file_path: str, list_of_messages: list):
    """Modification(bit messy) of the read_to_queue function, 
    such that it writes individual AIS messages to a list.
    Why? Because I thought there was no reason to divide into chunks first.
    Please correct me if I'm wrong:)


    Args:
        file_path (str): [path to the data]
        item_list (list): [list into which AIS messages will be stored]
    """
    with open(file_path, 'r') as data_lines:
        line = ""
        last_line = ""
        full_message = ""
        while True:
            line = data_lines.readline().rstrip() #read line and strip /n
            try:
                if line[0] != "!": #would throw IndexError at end of data
                    full_message = last_line + " " + line
                elif last_line != "": #prevent first line from being an empty string
                    list_of_messages.append(last_line)
                    full_message = line
                else:
                    full_message = line
            except IndexError:
                list_of_messages.append(full_message)
                break
            if not line:
                list_of_messages.append(last_line)
                break
            last_line = ""
            while True:
                    next_line = data_lines.readline().rstrip()
                    try: 
                        if next_line[0] != "!": #IndexError at end of data
                            full_message = full_message + " " + next_line
                        else:
                            list_of_messages.append(full_message)
                            last_line = next_line
                            break
                    except IndexError:
                            break


def do_something(data: str) -> str: 
    """Silly function reversing the ais message.

    Args:
        data (list): list of ais messages

    Returns:
        list: 'processed' data
    """
    results = ""
    
    reversed = data[::-1]
    results = reversed
    
    return results

def process_serial(function, data: list):

    for item in data:
        return function(data)

def main():

    import time

    start = time.perf_counter()

    file_path = "../data/synthetic_data.txt"

    list_of_messages = []

    read_to_list(file_path, list_of_messages)

    results = process_serial(do_something, list_of_messages)

    print(results)

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start,2)} seconds')

if __name__ == '__main__':
    main()
