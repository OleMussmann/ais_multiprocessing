#!/usr/bin/env python3


class Ship:
    known_ships = {}

    def __init__(self, line_number, filtered_message, logging_function):
        line_number = line_number
        data = filtered_message
        ship_id = data['mmsi']

        if ship_id in self.known_ships:
            self.known_ships[ship_id].update(line_number, data,
                                             logging_function)
        else:
            self.specs = {}
            self.known_ships[ship_id] = self
            self.ship_id = ship_id
            self.update(line_number, data, logging_function)

    def update(self, line_number, data, logging_function):
        for key in data:
            value = data[key]
            if not value:
                continue
            if key == "draught" and value == "0.0":
                continue
            self.specs[key] = value
        self.line_number = line_number
        logging_function("data line %s updated ship ID: %s",
                         line_number, self.ship_id)


def main():
    import messages
    import get_logger

    logger = get_logger.get_logger(__name__)

    first_line = "2017-01-24T00:00:00Z;!ABVDM,2,1,9,B,53nFBv01SJ<thHp622" + \
        "0H4heHTf2222222222221?50:454o<`9QSlUDp,0*09"
    second_line = "!ABVDM,2,2,9,B,888888888888880,2*2E"

    message = messages.Message(line_number=1, message_content=first_line)
    message.update(second_line)

    message.decode(logger.debug)

    message.filter(logger.debug)

    if message.is_usable_ais():
        # create ship
        Ship(message.line_number, message.filtered, logger.debug)

        for ship_id in Ship.known_ships:
            print(Ship.known_ships[ship_id].line_number)
            print(Ship.known_ships[ship_id].ship_id)
            print(Ship.known_ships[ship_id].specs)


if __name__ == "__main__":
    main()
