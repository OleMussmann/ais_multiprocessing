#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging

import config


def get_logger(name):
    """ Create a logger configured with settings from the CONFIG_FILE.

    Args:
        name (str): Name of the logger. `__name__` is highly recommended.
    Returns:
        logging.Logger: configured logger
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(config.log_format)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(config.console_logging_level)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    file_handler = logging.FileHandler(config.log_file)
    file_handler.setLevel(config.file_logging_level)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger


def main():
    logger = get_logger(__name__)
    logger.info("my info message")


if __name__ == "__main__":
    main()
