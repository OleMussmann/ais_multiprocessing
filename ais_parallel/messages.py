#!/usr/bin/env python3

import ais
import config
import importlib
import logging

# `libais` - the package of `ais` - has obnoxious logging that I don't know how
# to disable. Reloading logging fixes that, but may cause problems in `libais`.
# Remove the reload if `libais` breaks. Only reload logging once, right after
# importing everything!
importlib.reload(logging)


def is_message_valid(line_number, message, logging_function):
    message_valid = True
    try:
        if message['id'] not in config.valid_message_ids:
            logging_function("data line %s has invalid 'id' %s",
                             line_number, message['id'])
            message_valid = False

        if len(str(message['mmsi'])) != config.mmsi_length:
            logging_function("data line %s has 'mmsi' %s, which is not "
                             "of length 9", line_number, str(message['mmsi']))
            message_valid = False

        if message['mmsi'] in config.invalid_mmsis:
            logging_function("data line %s has invalid 'mmsi' %s",
                             line_number, message['mmsi'])
            message_valid = False

        if message['id'] == 1:
            if not (config.latitude_interval[0] <= message['x']
                    <= config.latitude_interval[1]):
                logging_function(
                    "data line %s has invalid latitude %s not in interval %s",
                    line_number, message['x'], config.latitude_interval)
                message_valid = False
            if not (config.longitude_interval[0] <= message['y']
                    <= config.longitude_interval[1]):
                logging_function(
                    "data line %s has invalid longitude %s not in interval %s",
                    line_number, message['y'], config.longitude_interval)
                message_valid = False
            if float(message['sog']) <= config.min_speed_over_ground:
                logging_function(
                    "data line %s's speed_over_ground %s is below cut-off %s",
                    line_number, message['sog'], config.min_speed_over_ground)
                message_valid = False

    except KeyError as error:
        logging_function("data line %s causes KeyError: %s",
                         line_number, error)
        message_valid = False

    return message_valid


def filter_message(line_number, message, timestamp, logging_function):
    if not is_message_valid(line_number, message, logging_function):
        return None

    try:
        filtered = {}
        if message['id'] in (1, 2, 3):
            filtered['id'] = message['id']
            filtered['mmsi'] = message['mmsi']
            filtered['x'] = round(message['x'], 4)
            filtered['y'] = round(message['y'], 4)
            filtered['position_accuracy'] = message['position_accuracy']
            filtered['speed_over_ground'] = round(message['sog'], 4)
            filtered['course_over_ground'] = round(message['cog'], 4)
            filtered['nav_status'] = message['nav_status']
            filtered['timestamp'] = timestamp

        if message['id'] == 5:
            filtered['id'] = message['id']
            filtered['mmsi'] = message['mmsi']
            filtered['imo_number'] = message['imo_num']
            filtered['vessel_name'] = message['name'].strip(' @')
            filtered['dimension_1'] = (float(message['dim_a']) +
                                       float(message['dim_b'])) * 10
            filtered['dimension_2'] = (float(message['dim_c']) +
                                       float(message['dim_d'])) * 10
            filtered['draught'] = round(float(message['draught']) * 10, 1)
            filtered['type_and_cargo'] = message['type_and_cargo']
            filtered['destination'] = message['destination'].strip(' @')
            filtered['timestamp'] = timestamp

        if message['id'] == 8:
            filtered['mmsi'] = message['mmsi']
            filtered['european_id'] = message['eu_id']
            filtered['length'] = round(float(message['length']) * 10, 0)
            filtered['beam'] = round(float(message['beam']) * 10, 0)
            filtered['draught'] = round(float(message['draught']), 1)
            filtered['ship_type'] = message['ship_type']
            filtered['is_loaded'] = message['loaded']
            filtered['hazardous_cargo'] = message['haz_cargo']
            filtered['timestamp'] = timestamp

        return filtered

    except KeyError as error:
        logging_function("data line %s causes KeyError: %s",
                         line_number, error)
        return None


class Message:
    def __init__(self, line_number, message_content):
        self.valid_codes = config.valid_codes
        self.valid_ais_starts = config.valid_ais_starts

        self.timestamp = message_content[:20]

        self.line_number = line_number

        self.code, self.total_parts, self.part_number, _, _, self.ais_data, \
            self.padding = message_content[21:].split(',')

        self.decoded = None
        self.filtered = None

    def update(self, update_content):
        next_ais_data, next_padding = update_content.split(',')[5:7]
        self.ais_data += next_ais_data  # append AIS data
        self.padding = next_padding  # replace padding

    def is_usable_ais(self):
        return (self.code in self.valid_codes) and \
            (self.ais_data[0] in self.valid_ais_starts)

    def decode(self, logging_function):
        try:
            self.decoded = ais.decode(self.ais_data,
                                      int(self.padding.split('*')[0]))
            return True
        except ais.DecodeError as error:
            logging_function("data line %s raises decoding error %s",
                             self.line_number, error)
            return False


    def filter(self, logging_function):
        self.filtered = filter_message(self.line_number, self.decoded,
                                       self.timestamp, logging_function)


def main():
    import get_logger

    logger = get_logger.get_logger(__name__)

    first_line = "2017-01-24T00:00:00Z;!ABVDM,2,1,9,B,53nFBv01SJ<thHp622" + \
        "0H4heHTf2222222222221?50:454o<`9QSlUDp,0*09"
    second_line = "!ABVDM,2,2,9,B,888888888888880,2*2E"

    message = Message(line_number=1, message_content=first_line)

    could_decode = message.decode(logger.info)
    print("Could decode: ", could_decode)

    message.update(second_line)

    print("Message usable: ", message.is_usable_ais())

    could_decode = message.decode(logger.info)
    print("Could decode: ", could_decode)

    message.filter(logger.info)
    filtered = message.filtered
    print("Filtered content: ", filtered)


if __name__ == "__main__":
    main()
