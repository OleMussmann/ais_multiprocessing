#!/usr/bin/env python3

from typing import Optional

try:
    from ais_parallel import messages
except ModuleNotFoundError:
    import messages

def read_to_queue(file_path, char_chunk_size, item_queue):
    previous_line_number = 0
    with open(file_path, 'r') as data_lines:
        last_line = []
        iterations = 0
        while True:
            iterations += 1
            lines = data_lines.readlines(char_chunk_size)
            chunk = last_line + lines
            if not lines:
                if last_line:
                    enumerated_line = [(previous_line_number + 1,
                                        last_line[0])]
                    item_queue.put(enumerated_line)
                break
            last_line = []
            while True:
                try:
                    next_line = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] == "!":  # line belongs to previous line
                    chunk.append(next_line)
                else:
                    last_line = [next_line]
                    break
            start_line_number = previous_line_number + 1
            end_line_number = start_line_number + len(chunk)
            line_numbers = list(range(start_line_number, end_line_number))
            enumerated_lines = list(zip(line_numbers, chunk))
            item_queue.put(enumerated_lines)
            previous_line_number = line_numbers[-1]


def queue_to_generator(item_queue, sentinel, number_of_sentinels):
    detected_sentinels = 0
    while True:
        item = item_queue.get()
        if item == sentinel:
            detected_sentinels += 1
            if detected_sentinels == number_of_sentinels:
                break
            continue
        yield item


def AIS_generator(data):
    end_of_data = False
    row_complete = False

    try:
        line_number, row = next(data)
    except StopIteration:
        return

    message = messages.Message(line_number, row)

    while True:
        try:
            line_number, row = next(data)
            # The current row is complete if the next one does not start with
            # an exclamation mark:
            row_complete = bool(row[0] != "!")
        except StopIteration:
            end_of_data = True
            row_complete = True

        if row_complete:
            yield message

            if end_of_data:
                return

            message = messages.Message(line_number, row)
        else:  # row not complete, update message with new content
            message.update(row)


def main():
    from multiprocessing import Queue

    file_path = "../data/synthetic_data.txt"
    char_chunk_size = 10000

    sentinel = "DONE"

    queue = Queue()

    def func(item):
        return item

    read_to_queue(file_path, char_chunk_size, queue)

    queue.put(sentinel)

    gen = queue_to_generator(queue, sentinel, number_of_sentinels=1)

    counter: Optional[int] = None

    line_numbers = []
    for counter, item in enumerate(gen, 1):  # first item is counted as 1
        enumerated_lines = func(item)
        for line_number, _ in enumerated_lines:
            line_numbers.append(line_number)

    print("Line numbers are consecutive:")
    print(line_numbers == list(range(1, 141087)))
    print()
    print("Number of chunks:")
    print(counter)


if __name__ == "__main__":
    main()
