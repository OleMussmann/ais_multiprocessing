#!/usr/bin/env python3

""" PSEUDO CODE CONCEPT """

def main():

    # from multiprocessing import Value
    # from ctypes import c_bool

    # workers_idle = [Value(c_bool, True)] * number_of_output_workers
    #
    # START job for single input process
    #
    # while True:
    #   if all([indicator.value for indicator in workers_idle]):
    #       chop a data block into chunks
    #
    #       send chunks to input_queue
    #
    #       if there are no chunks left, send end_sentinel instead
    #
    #       for _ in range(number_of_decoding_workers):
    #           input_queue.put(decoding_sentinel)
    #
    #       workers_idle.value = False
    #
    # END job for input process

    # START job for decoding_workers
    #
    # decode messages
    #
    # append (line_number, filtered_message) to a ship_queue, depending on
    # the mmsi
    #
    # if block is end_sentinel, append end_sentinel instead
    #
    # if block is finished, append decoding_sentinel
    #
    # if block was end_sentinel:
    #     break  # exit decoding worker
    #
    # END job for workers

    # START job for ship workers
    #
    # def ship_worker_tasks(ship_queue, decoding_sentinel, end_sentinel,
    #                         idle_boolean):
    #     while True:
    #         while (number_of_decoding_sentinels <
    #                number_of_decoding_workers):
    #             get blocks from ship_queue
    #             if block == decoding_sentinel:
    #                 number_of_decoding_sentinels += 1
    #                 continue
    #             if block == end_sentinel:
    #                 number_of_end_sentinel_blocks += 1
    #
    #             if number_of_end_sentinel_blocks ==
    #                      number_of_decoding_workers:
    #                 break
    #
    #         number_of_decoding_sentinels = 0
    #
    #         set idle_boolean=True  # start next round of data-blocks
    #
    #         if block == end_sentinel:
    #             break
    #
    #         sort blocks by line numbers or block numbers, so that all of them
    #         are in chronological order
    #
    #         create Ship() objects, infer journey, etc.
    #
    #     write output to file
    #
    #
    # ship_processes = []
    #
    # for worker_number in range(number_of_ship_workers):
    #
    #     idle_boolean = workers_idle[worker_number]
    #
    #     ship_process = Process(target=worker_function, args=(input_queue,
    #                   ship_queue, sentinel, idle_boolean))
    #
    #     ship_processes.append(ship_processes)
    #
    #     ship_process.start()
    #
    # END job for ship process

    # for decoding_process in decoding_processes:
    #     decoding_process.join()
    #
    # for ship_process ship_processes:
    #     ship_process.join()


if __name__ == "__main__":
    main()
