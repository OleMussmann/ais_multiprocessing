import logging


# logging
log_file = "./ais.log"
console_logging_level = logging.INFO
file_logging_level = logging.INFO
log_format = '%(levelname)-8s ░ %(asctime)s ░ %(name)s ' + \
             '%(filename)s line %(lineno)d ▓ %(message)s'

# ais_validation
valid_codes = ("!ABVDM", "!BSVDM")

valid_ais_starts = ('1', '2', '3', '4', '5', '8')
valid_message_ids = (1, 2, 3, 5, 8)  # are they the same?

# message_validation
mmsi_length = 9
#latitude_interval = [50.0, 55.0]
#longitude_interval = [-2.0, 9.0]
#latitude_interval = [-90.0, 90.0]
longitude_interval = [-180., 180.]
latitude_interval = [-180., 180.]
#longitude_interval = [-90.0, 90.0]
min_speed_over_ground = 0.2
invalid_mmsis = ["000000000"]
