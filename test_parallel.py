#!/usr/bin/env python3

from multiprocessing import Queue, Process
import time

#import get_logger
import ais_parallel as ap


def main():
    file_path = "./data/AIS-sample_part_0"
    char_chunk_size = int(1e5)

    sentinel = "DONE"

    number_of_workers = 6

    errors = 0
    not_decodable = 0
    messages_used = 0
    filter_not_passed = 0

    start = time.time()

    #logger = get_logger.get_logger(__name__)

    def logging_function(*args, **kwargs):
        #logger.debug(*args, **kwargs)
        pass

    def work(input_queue, output_queue, sentinel):
        while True:
            chunk = input_queue.get()
            if chunk == sentinel:
                break

            message_generator = ap.AIS_generator(iter(chunk))
            output = []
            for message in message_generator:
                if message.is_usable_ais():
                    decodable = message.decode(logging_function)
                    if decodable:
                        message.filter(logging_function)

                if message.filtered:
                    #output.append(message)
                    output.append((message.line_number, message.filtered))
            output_queue.put(output)
        output_queue.put(sentinel)

    print('1', flush=True)
    input_queue = Queue()
    output_queue = Queue()

    ap.read_to_queue(file_path, char_chunk_size, input_queue)

    print('2', flush=True)
    for _ in range(number_of_workers):
        input_queue.put(sentinel)

    processes = []

    print('3', flush=True)
    for _ in range(number_of_workers):
        process = Process(target=work, args=(input_queue, output_queue,
                                             sentinel))
        processes.append(process)
        process.start()

    print('4', flush=True)
    for process in processes:
        process.join(timeout=0)

    #work(input_queue, output_queue, sentinel)

    print('5', flush=True)
    output_generator = ap.queue_to_generator(output_queue, sentinel,
                                               number_of_workers)

    print('6', flush=True)

    intermediate = time.time()

    for chunk in output_generator:
        for line_number, filtered_message in chunk:
            try:
                s = ap.ships.Ship(line_number, filtered_message,
                                  logging_function)
                messages_used += 1
            except TypeError:
                errors += 1


    print('7', flush=True)


    end = time.time()
    print('errors: ', errors)
    print('not decodable: ', not_decodable)
    print('filter not passed: ', filter_not_passed)
    print('messages used: ', messages_used)
    print('known ships: ', len(s.known_ships))
    print("output step took ", end - intermediate)
    print("all took ", end - start)


if __name__ == "__main__":
    main()
