#!/usr/bin/env python3

import csv
import timeit
from functools import partial

ais_data_file = "../data/ais_one_line.txt"


def split_by_csv(file):
    with open(file) as f:
        reader = csv.reader(f, delimiter=',')
        row = next(reader)
    return row


def split_by_split(file):
    with open(file) as f:
        line = f.readline().rstrip('\n')

    return(line.split(','))


def main():

    print(split_by_csv(ais_data_file))
    print(split_by_split(ais_data_file))

    print(timeit.timeit(partial(split_by_csv, ais_data_file),
                        number=int(1e5)))

    print(timeit.timeit(partial(split_by_split, ais_data_file),
                        number=int(1e5)))


if __name__ == "__main__":
    main()
