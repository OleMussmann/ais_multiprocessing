import configparser

conf = configparser.ConfigParser()
conf.read("./config.ini")

log_file = conf.get('settings', 'log_file')
