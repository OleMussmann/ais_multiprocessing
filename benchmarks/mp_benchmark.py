#!/usr/bin/env python3

""" Standalone script to benchmark read- and chunking-speeds.

    The data is structured such that a "data-block" consists of a line starting
    with an exclamation mark "!" plus optional following lines _not_ starting
    with an exclamation mark.


    ! 5 7 9 ] <- data block
    ! 5 7 9 ┐
    5 7 9   ├ <- data block
    5 7 9   ┘
    ! 5 7 9 ] <- data block

    When chunking the data for multiprocessing, it must not divide data blocks.

    `read_with_readlines` reads a certain number of bytes from a file at the
    same time. Assumed to be the fastest way to read and chunk data.

    `read_with_next` reads lines one-by-one. Assumed to be slightly slower than
    read_with_readlines.

    `read_all` does not chunk. By comparing the other function's speeds to this
    function, we can infer the overhead through reading and chunking.
"""

from typing import List, TextIO, Tuple
import functools
from timeit import Timer
from queue import Empty
from multiprocessing import Process, Queue
from multiprocessing.managers import BaseProxy

SENTINEL = "DONE"

FILE_PATH: str = "../data/synthetic_data.txt"

def read_with_readlines(file_path: str, chars: int,
                        item_queue: BaseProxy) -> None:
    """Read (at least) a certain number of bytes, plus the rest of the current
    line, plus the rest of the data block (see script docstring) if applicable.
    Chunk these lines, leaving data blocks intact. Put the data chunks in a
    queue.

    Args:
        file_path (str): File to read
        chars (int): Minimum number of bytes per chunk
        item_queue (Queue): Queue to be filled

    Returns:
        None:
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        last_line: List[str] = []
        iterations: int = 0
        while True:
            iterations += 1
            lines: List[str] = data_lines.readlines(chars)
            chunk: List[str] = last_line + lines
            if not lines:
                item_queue.put(last_line)
                break
            last_line: List[str] = []
            while True:
                try:
                    next_line: str = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] != "!":
                    chunk.append(next_line)
                else:
                    last_line: List[str] = [next_line]
                    break
            item_queue.put(chunk)

    item_queue.put(SENTINEL)


def read_all(file_path: str, item_queue: BaseProxy):
    """Read a complete file and put the content into a queue.

    Args:
        file_path (str): File to be read
        item_queue (Queue): Queue to be filled
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        lines: list = data_lines.readlines()
        item_queue.put(lines)

    item_queue.put(SENTINEL)


def assemble_queue(item_queue: Queue) -> Tuple[list, int]:
    """Read extract the items of a queue and assemble them into a flat list.
    Then we can compare the output of the raw `read_all` read function with the
    read- and chunking-functions `read_with_readlines` and `read_with_next`.

    Args:
        item_queue (Queue): Queue to be read

    Returns:
        Tuple[list, int]: Tuple of the queue content and the queue length.
    """
    queue_items: list = []
    items_in_queue: int = 0
    item: list
    while True:
        try:
            item = item_queue.get(timeout=.1)
            if item == SENTINEL:
                break
            queue_items.append(item)
        except Empty:
            print("no read")
            break
        items_in_queue += 1

    flat_list: list = [x for sublist in queue_items for x in sublist]
    return flat_list, items_in_queue


def do_something(data: List[str]) -> str:
    """Silly function reversing the ais message.

    Args:
        data (List[str]): list of ais messages

    Returns:
        list: 'processed' data
    """

    return reversed(data)


def multi_process(function, queue):
    processes = [Process(target=function, args=(queue)) for x in range(4)]

    # Run processes
    for p in processes:
        p.start()

    # Exit the completed processes
    for p in processes:
        p.join()

    # Get process results from the output queue
    results = [output.get() for p in processes]

    print(results)
    return


def main():
    """ Main function, run this for benchmarking.
    """

    number_of_benchmark_runs: int = 10

    # At least how many chars to read at once in read_with_readlines()
    chars: int = 75000

    test_queue: Queue = Queue()

    read_all(FILE_PATH, test_queue)
    raw_content: list
    raw_content, _ = assemble_queue(test_queue)

    read_with_readlines(FILE_PATH, chars, test_queue)
    rl_content: list
    rl_iterations: int
    rl_content, rl_iterations = assemble_queue(test_queue)

    timer_readlines: Timer = Timer(functools.partial(
        read_with_readlines, FILE_PATH, chars, test_queue))
    runtime_readlines: float = timer_readlines.timeit(number_of_benchmark_runs)

    timer_raw: Timer = Timer(functools.partial(
        read_all, FILE_PATH, test_queue))
    runtime_raw: float = timer_raw.timeit(number_of_benchmark_runs)

    # `flush=True` is necessary to prevent a BrokenPipeError
    # Why that one is raised in the first place is beyond me
    print(f"Readlines iterations: {rl_iterations}")
    print()
    print("Do read_with_readlines() and read_raw() produce the same result? " +
          f"{rl_content == raw_content}")
    print()
    print(f"{number_of_benchmark_runs} runs of read_with_readlines() took: "
          f"{runtime_readlines:.4f} s")
    print(f"{number_of_benchmark_runs} runs of read_raw() took: "
          f"{runtime_raw:.4f} s", flush=True)


if __name__ == "__main__":
    main()
