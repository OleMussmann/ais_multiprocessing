#!/usr/bin/env python3

import configparser
import timeit
import config_file
import config_module
from functools import partial

conf = configparser.ConfigParser()
conf.read("./config.ini")

def conf_with_py_import():
    return config_file.log_file

def conf_with_file_import():
    return config_module.log_file

def conf_with_configparser():
    return conf.get('settings', 'log_file')

def conf_with_configparser_and_dict(conf_dict):
    return conf_dict['log_file']

def main():

    conf_dict = {}
    conf_dict['log_file'] = conf.get('settings', 'log_file')

    print(timeit.timeit(conf_with_configparser))
    print(timeit.timeit(conf_with_py_import))
    print(timeit.timeit(conf_with_file_import))
    print(timeit.timeit(partial(conf_with_configparser_and_dict, conf_dict)))


if __name__ == "__main__":
    main()
