#!/usr/bin/env python3

import timeit
from functools import partial

row = "2017-01-24T00:00:00Z;!ABVDM,1,1,,B,B43JRq00LhTWc5VejDI>wwWUoP06,0*29"


def check_first_by_startswith(data_row):
    return data_row.startswith("2")


def check_first_by_index(data_row):
    return data_row[0] == "2"


def main():
    print(timeit.timeit(partial(check_first_by_startswith, row)))

    print(timeit.timeit(partial(check_first_by_index, row)))


if __name__ == "__main__":
    main()
