#!/usr/bin/env python3

import timeit
from functools import partial

row = "2017-01-24T00:00:00Z;!ABVDM,1,1,,B,B43JRq00LhTWc5VejDI>wwWUoP06,0*29"


def make_array(data_row):
    timestamp, message = data_row.split(";")
    return timestamp, message


def split_by_index(data_row):
    timestamp = data_row[:20]
    message = data_row[21:]
    return timestamp, message


def main():
    print(timeit.timeit(partial(make_array, row)))

    print(timeit.timeit(partial(split_by_index, row)))


if __name__ == "__main__":
    main()
