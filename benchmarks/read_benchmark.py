#!/usr/bin/env python3

""" Standalone script to benchmark read- and chunking-speeds.

    The data is structured such that a "data-block" consists of a line starting
    with an exclamation mark "!" plus optional following lines _not_ starting
    with an exclamation mark.


    ! 5 7 9 ] <- data block
    ! 5 7 9 ┐
    5 7 9   ├ <- data block
    5 7 9   ┘
    ! 5 7 9 ] <- data block

    When chunking the data for multiprocessing, it must not divide data blocks.

    `read_with_readlines` reads a certain number of bytes from a file at the
    same time. Assumed to be the fastest way to read and chunk data.

    `read_with_next` reads lines one-by-one. Assumed to be slightly slower than
    read_with_readlines.

    `read_all` does not chunk. By comparing the other function's speeds to this
    function, we can infer the overhead through reading and chunking.
"""

from typing import List, TextIO, Tuple, Union
import functools
from timeit import Timer
from queue import Empty
from multiprocessing import Queue, Manager
from multiprocessing.managers import BaseProxy, SyncManager

SENTINEL = "DONE"

FILE_PATH: str = "../data/synthetic_data.txt"

def read_with_readlines(file_path: str, chars: int,
                        item_queue: BaseProxy) -> None:
    """Read (at least) a certain number of bytes, plus the rest of the current
    line, plus the rest of the data block (see script docstring) if applicable.
    Chunk these lines, leaving data blocks intact. Put the data chunks in a
    queue.

    Args:
        file_path (str): File to read
        chars (int): Minimum number of bytes per chunk
        item_queue (Queue): Queue to be filled

    Returns:
        None:
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        last_line: List[str] = []
        iterations: int = 0
        while True:
            iterations += 1
            lines: List[str] = data_lines.readlines(chars)
            chunk: List[str] = last_line + lines
            if not lines:
                item_queue.put(last_line)
                break
            last_line: List[str] = []
            while True:
                try:
                    next_line: str = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] != "!":
                    chunk.append(next_line)
                else:
                    last_line: List[str] = [next_line]
                    break
            item_queue.put(chunk)

    item_queue.put(SENTINEL)


def read_with_next(file_path: str, number_of_lines: int,
                   item_queue: BaseProxy) -> None:
    """Read a certain number of lines, one by one. Chunk these lines, leaving
    data blocks (see script docstring) intact. Put the data chunks in a queue.

    Args:
        file_path (str): File to read
        number_of_lines (int): Minimum number of lines per chunk
        item_queue (Queue): Queue to be filled

    Returns:
        None:
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        last_line: List[str] = []
        iterations: int = 0
        while True:
            iterations += 1
            lines: List[str] = []
            for _ in range(number_of_lines):
                try:
                    lines.append(next(data_lines))
                except StopIteration:
                    break
            chunk: List[str] = last_line + lines
            if not lines:
                item_queue.put(last_line)
                break
            last_line: List[str] = []
            while True:
                try:
                    next_line: str = next(data_lines)
                except StopIteration:
                    break
                if next_line[0] != "!":
                    chunk.append(next_line)
                else:
                    last_line: List[str] = [next_line]
                    break
            item_queue.put(chunk)

    item_queue.put(SENTINEL)


def read_all(file_path: str, item_queue: Queue):
    """Read a complete file and put the content into a queue.

    Args:
        file_path (str): File to be read
        item_queue (Queue): Queue to be filled
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        lines: list = data_lines.readlines()
        item_queue.put(lines)

    item_queue.put(SENTINEL)


def read_raw(file_path: str):
    """Read and return a complete file.

    Args:
        file_path (str): File to be read
    """
    data_lines: TextIO
    with open(file_path, 'r') as data_lines:
        lines: list = data_lines.readlines()

    return lines


def assemble_queue(item_queue: Queue) -> Tuple[list, int]:
    """Read extract the items of a queue and assemble them into a flat list.
    Then we can compare the output of the raw `read_all` read function with the
    read- and chunking-functions `read_with_readlines` and `read_with_next`.

    Args:
        item_queue (Queue): Queue to be read

    Returns:
        Tuple[list, int]: Tuple of the queue content and the queue length.
    """
    item_list: list = []
    items_in_queue: int = 0
    item: list
    while True:
        try:
            item = item_queue.get(timeout=.1)
            if item == SENTINEL:
                break
            item_list.append(item)
        except Empty:
            print("no read")
            break
        items_in_queue += 1

    flat_list: list = [x for sublist in item_list for x in sublist]
    return flat_list, items_in_queue


def emtpy_queue(queue: Union[Queue, BaseProxy]) -> None:
    while not queue.empty():
        queue.get()


def main():
    """ Main function, run this for benchmarking.
    Raises: BrokenPipeError for unknown reasons
    """

    number_of_benchmark_runs: int = 10

    # For benchmarking, adjust these so that the "iterations" of both
    # read_with_readlines() and read_with_next() are similar.
    # The ratio depends on the number of chars per line in the data file.
    #
    # How many lines to read at once in read_with_next()
    number_of_lines: int = 10000
    # At least how many chars to read at once in read_with_readlines()
    chars: int = 75000

    queue: Queue = Queue()
    manager: SyncManager = Manager()
    manager_queue: BaseProxy = manager.Queue()

    for q in [queue, manager_queue]:

        raw_content = read_raw(FILE_PATH)

        read_all(FILE_PATH, q)
        all_content: list
        all_content, _ = assemble_queue(q)

        emtpy_queue(q)

        read_with_readlines(FILE_PATH, chars, q)
        rl_content: list
        rl_iterations: int
        rl_content, rl_iterations = assemble_queue(q)

        emtpy_queue(q)

        read_with_next(FILE_PATH, number_of_lines, q)
        n_content: list
        n_iterations: int
        n_content, n_iterations = assemble_queue(q)

        emtpy_queue(q)

        timer_readlines: Timer = Timer(functools.partial(
            read_with_readlines, FILE_PATH, chars, q))
        runtime_readlines: float = timer_readlines.timeit(
            number_of_benchmark_runs)

        emtpy_queue(q)

        timer_next: Timer = Timer(functools.partial(
            read_with_next, FILE_PATH, number_of_lines, q))
        runtime_next: float = timer_next.timeit(number_of_benchmark_runs)

        emtpy_queue(q)

        timer_all: Timer = Timer(functools.partial(
            read_all, FILE_PATH, q))
        runtime_all: float = timer_all.timeit(number_of_benchmark_runs)

        emtpy_queue(q)

        timer_raw: Timer = Timer(functools.partial(
            read_raw, FILE_PATH))
        runtime_raw: float = timer_raw.timeit(number_of_benchmark_runs)

        print()
        print("multiprocessing Queue type: " + str(type(q)))
        print(f"Readlines iterations: {rl_iterations}")
        print(f"Next iterations: {n_iterations}")
        print()
        print("Do read_with_readlines() and read_raw() produce the same result? " +
              f"{rl_content == raw_content}")
        print("Do read_with_next() and read_raw() produce the same result? " +
              f"{n_content == raw_content}")
        print("Do read_all() and read_raw() produce the same result? " +
              f"{all_content == raw_content}")
        print()
        print(f"{number_of_benchmark_runs} runs of read_with_readlines() took: "
              f"{runtime_readlines:.4f} s")
        print(f"{number_of_benchmark_runs} runs of read_with_next() took: "
              f"{runtime_next:.4f} s")
        print(f"{number_of_benchmark_runs} runs of read_all() took: "
              f"{runtime_all:.4f} s")
        print(f"{number_of_benchmark_runs} runs of read_raw() took: "
              f"{runtime_raw:.4f} s")


if __name__ == "__main__":
    main()
