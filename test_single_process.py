#!/usr/bin/env python3

from multiprocessing import Queue
import time

import ais_parallel as ap


def main():
    file_path = "./data/AIS-sample_part_0"
    char_chunk_size = 20000

    sentinel = "DONE"
    errors = 0
    not_decodable = 0
    messages_used = 0
    filter_not_passed = 0

    start = time.time()

    logger = ap.get_logger(__name__)


    def logging_function(*args, **kwargs):
        #logger.debug(*args, **kwargs)
        pass


    print('1', flush=True)
    input_queue = Queue()
    output_queue = Queue()

    ap.read_to_queue(file_path, char_chunk_size, input_queue)
    input_queue.put(sentinel)

    print('2', flush=True)
    input_generator = ap.queue_to_generator(input_queue, sentinel,
                                              number_of_sentinels=1)

    print('3', flush=True)
    for chunk in input_generator:
        message_generator = ap.AIS_generator(iter(chunk))
        output = []
        for message in message_generator:
            if message.is_usable_ais():
                decodable = message.decode(logging_function)
                if decodable:
                    message.filter(logging_function)
                else:
                    not_decodable += 1

            if message.filtered:
                output.append((message.line_number, message.filtered))
                messages_used += 1
            else:
                filter_not_passed += 1
            #if decodable:
            #    output.append(message)
        output_queue.put(output)

    print('4', flush=True)
    output_queue.put(sentinel)

    print('5', flush=True)
    output_generator = ap.queue_to_generator(output_queue, sentinel,
                                               number_of_sentinels=1)

    intermediate = time.time()

    print('6', flush=True)
    for chunk in output_generator:
        for line_number, filtered_message in chunk:
            try:
                ap.Ship(line_number, filtered_message, logging_function)
            except TypeError:
                errors += 1

    print('7', flush=True)

    print('errors: ', errors)
    print('not decodable: ', not_decodable)
    print('filter not passed: ', filter_not_passed)
    print('messages used: ', messages_used)
    end = time.time()
    print('output step took ', end - intermediate)
    print("all took ", end - start)


if __name__ == "__main__":
    main()
